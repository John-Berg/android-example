package nl.jvdb.demoapplication.data.api;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface GitApi {
    @GET("users/{userName}/repos")
    Observable<Response<ResponseBody>> getGitRepositories(@Path("userName") String userName);
}
