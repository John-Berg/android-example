package nl.jvdb.demoapplication.data.repository;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import nl.jvdb.demoapplication.data.model.GitItem;

public class GitRepository {

    @Inject
    public GitRepository() {

    }

    public void insert(final List<GitItem> gitItems) {
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(gitItems);
            }
        });
    }

    public List<GitItem> findAll() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<GitItem> results = realm.where(GitItem.class).findAll();
        return realm.copyFromRealm(results);
    }
}
