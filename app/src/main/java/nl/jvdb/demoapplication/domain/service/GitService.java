package nl.jvdb.demoapplication.domain.service;

import java.util.List;

import nl.jvdb.demoapplication.data.model.GitItem;
import rx.Observable;

public interface GitService {
    Observable<List<GitItem>> getGitRepositories(int page, int size);
}
